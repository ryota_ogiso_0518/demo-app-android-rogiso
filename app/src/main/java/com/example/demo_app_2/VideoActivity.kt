package com.example.demo_app_2

import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.widget.MediaController
import android.widget.VideoView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_video.*

class VideoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        supportActionBar?.title = "Play Video"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)

        Handler(mainLooper).postDelayed({
            var moviePath = Uri.parse("android.resource://" + this.getPackageName() + "/" + R.raw.video01);
                videoView.setVideoURI(moviePath)

            videoView.setOnPreparedListener{
                videoView.start()

                videoView.setMediaController(MediaController(this))
            }

            videoView.setOnCompletionListener{
                //finish()
            }
        },200)
    }
    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}