package com.example.demo_app_2

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Layout
import android.util.Log
import android.widget.SeekBar
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_picker.*


class Picker : AppCompatActivity() {
    
    override fun onCreate(savedInstanceState: Bundle?) {
        supportActionBar?.title = "Slider Sample"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picker)

        val sb = listOf(seekBar,seekBar2,seekBar3)
        seekBar.max=255
        seekBar2.max=255
        seekBar3.max=255

        sb.forEach(){
            it.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{

                override fun onProgressChanged(
                    seekBar: SeekBar?,
                    progress: Int,
                    fromUser: Boolean
                ) {

                    sbChanged()
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) {
                }

                override fun onStopTrackingTouch(seekBar: SeekBar?) {
                }

            })
        }
        sbChanged()
    }
    fun sbChanged(){
//        fun Redraw(){
          textView.setBackgroundColor(Color.rgb(seekBar.progress,seekBar2.progress,seekBar3.progress))
        //}
    }
    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}
