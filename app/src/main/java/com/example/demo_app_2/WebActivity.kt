package com.example.demo_app_2

import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_web.*

class WebActivity: AppCompatActivity() {
     override fun onCreate(savedInstanceState: Bundle?) {
         supportActionBar?.setDisplayHomeAsUpEnabled(true)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
         val myWebView : WebView = findViewById(R.id.webView)
         //setContentView(myWebView)
         myWebView.loadUrl("https://www.sonix.asia/")
    }
    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}