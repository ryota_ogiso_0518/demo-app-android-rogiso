package com.example.demo_app_2

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil.setContentView
import kotlinx.android.synthetic.main.activity_form.*
import kotlinx.android.synthetic.main.activity_form_2.*

class FormActivity2 : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        supportActionBar?.title = "アンケート"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_2)

        val name_text = intent.getStringExtra("Name")
        val seibetsu_text = intent.getStringExtra("seibetu")
        val year = intent.getStringExtra("year")
        val month = intent.getStringExtra("month")
        val day = intent.getStringExtra("day")
        val age_text = intent.getStringExtra("age")
        val check_text = intent.getStringExtra("check")
        val check_text2 = intent.getStringExtra("check2")
        val check_text3 = intent.getStringExtra("check3")
        val check_text4 = intent.getStringExtra("check4")
        val check_text5 = intent.getStringExtra("check5")


        textView17.text = name_text
        textView18.text = seibetsu_text
        textView19.text = (year + "年" + month + "月" + day + "日")
        textView20.text = age_text
        textView21.text = check_text
        textView24.text = check_text2
        textView25.text = check_text3
        textView26.text = check_text4
        textView27.text = check_text5

        button2.setOnClickListener(){
            val intent = Intent(this@FormActivity2,MainActivity::class.java)
            startActivity(intent)
        }
    }
    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }


}