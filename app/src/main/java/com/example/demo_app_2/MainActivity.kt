package com.example.demo_app_2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.MapFragment

class MainActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    private var list = listOf<String>("Picker","Map","Video","WebView","View Pager","Form")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        viewManager = LinearLayoutManager(this)
        viewAdapter = TopAdapter(list)

        recyclerView = findViewById<RecyclerView>(R.id.simpleRecyclerView).apply {
            setHasFixedSize(true)

            layoutManager = viewManager

            adapter = viewAdapter

            (viewAdapter as TopAdapter).setOnItemClickListener(object:TopAdapter.OnItemClickListener{
                override fun onItemClickListener(view: View, position: Int, clickedText: String) {
                    Toast.makeText(applicationContext,clickedText,Toast.LENGTH_LONG).show()
                    if (clickedText == "Picker"){
                        val intent = Intent(this@MainActivity,Picker::class.java)
                        startActivity(intent)

                    }else if(clickedText == "Map"){
                        val intent = Intent(this@MainActivity,MapsActivity::class.java)
                        startActivity(intent)
                    }else if(clickedText == "WebView"){
                        val intent = Intent(this@MainActivity,WebActivity::class.java)
                        startActivity(intent)
                    }else if(clickedText == "View Pager"){
                        val intent = Intent(this@MainActivity,ViewPagerActivity::class.java)
                        startActivity(intent)
                    }else if(clickedText == "Form"){
                        val intent = Intent(this@MainActivity,FormActivity::class.java)
                        startActivity(intent)
                    }else if(clickedText == "Video"){
                        val intent = Intent(this@MainActivity,VideoActivity::class.java)
                        startActivity(intent)
                    }


                }
            })
        }
        //binding.recyclerView.adapter = adapter

        //adapter.setOnItemClickListener(object : MyAdapter.OnItemClickListener {
          //  override fun onClick(view: Int?, data: MyData) {
            //    Toast.makeText(applicationContext, data.name, Toast.LENGTH_LONG).show()
           // }
        //})
    }
}
