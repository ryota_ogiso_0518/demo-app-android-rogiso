package com.example.demo_app_2

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_form.*
import kotlinx.android.synthetic.main.activity_picker.*
import java.text.SimpleDateFormat
import java.util.*

open class FormActivity:AppCompatActivity() {

    private val spinnerItems = Array(71,{i ->  (1950+i).toString()})

    private val spinnerItems2 = Array(12,{i ->(1+i).toString()})

    private val spinnerItems3 = Array(31,{i -> (1+i).toString() })

    var today : String?= null

    override  fun onCreate(savedInstanceState: Bundle?) {
        supportActionBar?.title = "アンケート"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        fun getToday():String {
            val date = Date()
            val format = SimpleDateFormat("yyyy", Locale.getDefault())
            return format.format(date)
        }

        fun getMonth():String{
            val date = Date()
            val format = SimpleDateFormat("MM",Locale.getDefault())
            return format.format(date)
        }

        fun getDay():String{
            val date = Date()
            val format = SimpleDateFormat("dd",Locale.getDefault())
            return format.format(date)
        }

       super.onCreate(savedInstanceState)
       setContentView(R.layout.activity_form)

       val adapter = ArrayAdapter(applicationContext,android.R.layout.simple_spinner_item,spinnerItems)
        val adapter2 = ArrayAdapter(applicationContext,android.R.layout.simple_spinner_item,spinnerItems2)
        val adapter3 = ArrayAdapter(applicationContext,android.R.layout.simple_spinner_item,spinnerItems3)

       adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

       spinner.adapter = adapter
        spinner2.adapter = adapter2
        spinner3.adapter = adapter3

        var name_text:String? = null
        var seibetsu_text:String? = null
        var age_text:String? = null
        var check_text:String? = ""
        var check_text2:String? = ""
        var check_text3:String? = ""
        var check_text4:String? = ""
        var check_text5:String? = ""


        var y :String
        var i : Int? = null
        var j :Int? = null
        var m :String
        var d :String
        var M :Int? = null
        var D :Int
        var a :Int? = null
        var b :Int? = null
        var age :Int

        var item1 :String? = null
        var item2 : String? = null
        var item3 :String? = null

       spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
           override fun onItemSelected(
               parent: AdapterView<*>?,
               view: View?,
               position: Int,
               id: Long
           ) {
               val spinnerParent = parent as Spinner
                y  = getToday()
                i  = Integer.parseInt(y)
                item1 = spinnerParent.selectedItem as String
                j  = Integer.parseInt(item1!!)




           }
           override fun onNothingSelected(parent: AdapterView<*>?) {

           }
       }

        spinner2.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val spinnerParent = parent as Spinner
                item2 = spinnerParent.selectedItem as String
                a  = Integer.parseInt(item2!!)
                m  = getMonth()
                M  = Integer.parseInt(m)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

        spinner3.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val spinnerParent = parent as Spinner
                item3 = spinnerParent.selectedItem as String
                b  = Integer.parseInt(item3!!)
                d  = getDay()
                D  = Integer.parseInt(d)
                age = (i!! - j!!)
                if(M!! < a!! ){
                    age = age-1
                }else if(M!! == a!! && D!! <b!!){
                    age = age-1
                }
                age_text=age.toString()
                textView2.text = age_text
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

        checkBox6.isChecked = false
        checkBox7.isChecked = false
        checkBox8.isChecked = false
        checkBox9.isChecked = false
        checkBox10.isChecked = false

       button.setOnClickListener{
           if(editText.text != null){
               name_text = editText.text.toString()
           }

           val radioGroup: RadioGroup = findViewById(R.id.radiogroup)
           val id = radioGroup.checkedRadioButtonId
           val radioButton = radioGroup.findViewById<RadioButton>(id)
           seibetsu_text = radioButton.text.toString()
           val check = checkBox6.isChecked
           if(check){
               check_text += checkBox6.text.toString()
           }
           val check2 = checkBox7.isChecked()
           if(check2){
               check_text2 += checkBox7.text.toString()
           }
           val check3 = checkBox8.isChecked()
           if(check3){
               check_text3 += checkBox8.text.toString()
           }
           val check4 = checkBox9.isChecked()
           if(check4){
               check_text4 += checkBox9.text.toString()
           }
           val check5 = checkBox10.isChecked()
           if(check5){
               check_text5 += checkBox10.text.toString()
           }
           val intent = Intent(this@FormActivity,FormActivity2::class.java)
           intent.putExtra("Name",name_text)
           intent.putExtra("seibetu",seibetsu_text)
           intent.putExtra("year",item1)
           intent.putExtra("month",item2)
           intent.putExtra("day",item3)
           intent.putExtra("age",age_text)
           intent.putExtra("check",check_text)
           intent.putExtra("check2",check_text2)
           intent.putExtra("check3",check_text3)
           intent.putExtra("check4",check_text4)
           intent.putExtra("check5",check_text5)
           startActivity(intent)
       }
   }
    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}