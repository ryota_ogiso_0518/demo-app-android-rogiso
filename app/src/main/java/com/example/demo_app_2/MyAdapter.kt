//package com.example.demo_app_2
//
//import android.view.LayoutInflater
//import android.view.ViewGroup
//import androidx.recyclerview.widget.RecyclerView
//
//class MyAdapter(var dataList: List<MyData>) : RecyclerView.Adapter<MyAdapter.BindingHolder>() {
//    lateinit var listener: OnItemClickListener
//
//    //val it :Int? = null
//      fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): BindingHolder? {
//        setOnItemClickListener(listener)
//        val layoutInflater = LayoutInflater.from(parent!!.context)
//        val binding = OriginalItemLayoutBinding.inflate(layoutInflater, parent, false)
//        return BindingHolder(binding)
//    }
//
//    override fun onBindViewHolder(holder: BindingHolder, position: Int) {
//        val data = dataList[position]
//        holder.binding.setData(data)
//        holder.binding.originalLinearLayout.setOnClickListener({
//            listener.onClick(it,data)
//        })
//    }
//
//    override fun getItemCount(): Int {
//        return dataList.size
//    }
//
//    interface OnItemClickListener {
//        fun onClick(view: Int?, data: MyData)
//    }
//
//    fun setOnItemClickListener(listener: OnItemClickListener) {
//        this.listener = listener
//    }
//
//    class BindingHolder(var binding : OriginalItemLayoutBinding) : RecyclerView.ViewHolder(binding.root)
//
//    
//}